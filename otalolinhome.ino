#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Update.h>
#include <PubSubClient.h>
#include "DHT.h"

#define DHTTYPE DHT22

const char* host = "esp32";
const char* ssid = "Your SSID";
const char* password = "Your Password";
const char *mqtt_server = "Your Server IP";
WiFiClient lolinClient;
PubSubClient client(lolinClient);

//GPIO - Sensors
const int pir = 39;
const int ldr = 34;
const int DHTPin = 26;
DHT dht(DHTPin, DHTTYPE);

//GPIO - Relays

const int r1 = 13;
const int r2 = 25;
const int r3 = 32;
const int r4 = 33;


// Timers auxiliar variables
long now = millis();
long now2 = millis();
long lastMeasure = 0;
long lastHeartbeat = 0;
bool heartbeatServerOn = false;


WebServer server(80);

/*
   Login page
*/

const char* loginIndex =
  "<form name='loginForm'>"
  "<table width='20%' bgcolor='A09F9F' align='center'>"
  "<tr>"
  "<td colspan=2>"
  "<center><font size=4><b>ESP32 Login Page</b></font></center>"
  "<br>"
  "</td>"
  "<br>"
  "<br>"
  "</tr>"
  "<td>Username:</td>"
  "<td><input type='text' size=25 name='userid'><br></td>"
  "</tr>"
  "<br>"
  "<br>"
  "<tr>"
  "<td>Password:</td>"
  "<td><input type='Password' size=25 name='pwd'><br></td>"
  "<br>"
  "<br>"
  "</tr>"
  "<tr>"
  "<td><input type='submit' onclick='check(this.form)' value='Login'></td>"
  "</tr>"
  "</table>"
  "</form>"
  "<script>"
  "function check(form)"
  "{"
  "if(form.userid.value=='admin' && form.pwd.value=='admin')"
  "{"
  "window.open('/serverIndex')"
  "}"
  "else"
  "{"
  " alert('Error Password or Username')/*displays error message*/"
  "}"
  "}"
  "</script>";

/*
   Server Index Page
*/

const char* serverIndex =
  "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
  "<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
  "<input type='file' name='update'>"
  "<input type='submit' value='Update'>"
  "</form>"
  "<div id='prg'>progress: 0%</div>"
  "<script>"
  "$('form').submit(function(e){"
  "e.preventDefault();"
  "var form = $('#upload_form')[0];"
  "var data = new FormData(form);"
  " $.ajax({"
  "url: '/update',"
  "type: 'POST',"
  "data: data,"
  "contentType: false,"
  "processData:false,"
  "xhr: function() {"
  "var xhr = new window.XMLHttpRequest();"
  "xhr.upload.addEventListener('progress', function(evt) {"
  "if (evt.lengthComputable) {"
  "var per = evt.loaded / evt.total;"
  "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
  "}"
  "}, false);"
  "return xhr;"
  "},"
  "success:function(d, s) {"
  "console.log('success!')"
  "},"
  "error: function (a, b, c) {"
  "}"
  "});"
  "});"
  "</script>";

/*
   setup function
*/
void setup() {
  Serial.begin(115200);
  dht.begin();
  pinMode(r1, OUTPUT);
  pinMode(r2, OUTPUT);
  pinMode(r3, OUTPUT);
  pinMode(r4, OUTPUT);
  pinMode(pir, INPUT);
  pinMode(ldr, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void setup_wifi() {
  delay(10);
  long startupTime = millis();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  WiFi.setSleep(false);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (millis() - startupTime > 3000) {
      while (startupTime + 2000 > millis()) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(250);
        digitalWrite(LED_BUILTIN, LOW);
      }
      printf("Connection to wifi failed");

      reconnect();
    }
  }
  Serial.println("");
  Serial.print("Lolin Wemos 32 connected to WiFi  - IP address: ");
  Serial.println(WiFi.localIP());

  /*use mdns for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    Serial.println("Error setting up MDNS responder!");
    while (1) {
      delay(1000);
    }
  }
  Serial.println("mDNS responder started");
  /*return index page which is stored in serverIndex */
  server.on("/", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", loginIndex);
  });
  server.on("/serverIndex", HTTP_GET, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/html", serverIndex);
  });
  /*handling uploading firmware file */
  server.on("/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart();
  }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.printf("Update: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    }
  });
  server.begin();
}

void callback(String topic, byte *message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageTemp;

  for (int i = 0; i < length; i++) {
    Serial.print((char) message[i]);
    messageTemp += (char) message[i];
  }
  Serial.println();
  if (topic == "sys/heartbeat") {
    heartbeatServerOn = true;
    lastHeartbeat = millis();
  }

  if (topic == "room/r1") {
    Serial.print("Changing r1 to ");
    if (messageTemp == "on") {
      digitalWrite(r1, LOW);
      Serial.print("On");
    } else if (messageTemp == "off") {
      digitalWrite(r1, HIGH);
      Serial.print("Off");
    }
  }
  if (topic == "room/r2") {
    Serial.print("Changing r2 to ");
    if (messageTemp == "on") {
      digitalWrite(r2, LOW);
      Serial.print("On");
    } else if (messageTemp == "off") {
      digitalWrite(r2, HIGH);
      Serial.print("Off");
    }
  }
  if (topic == "room/r3") {
    Serial.print("Changing r3 to ");
    if (messageTemp == "on") {
      digitalWrite(r3, LOW);
      Serial.print("On");
    } else if (messageTemp == "off") {
      digitalWrite(r3, HIGH);
      Serial.print("Off");
    }
  }
  if (topic == "room/r4") {
    Serial.print("Changing r4 to ");
    if (messageTemp == "on") {
      digitalWrite(r4, LOW);
      Serial.print("On");
    } else if (messageTemp == "off") {
      digitalWrite(r4, HIGH);
      Serial.print("Off");
    }
  }

  Serial.println();
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    setup_wifi();
    client.setServer(mqtt_server, 1883);
    client.setCallback(callback);
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    boolean CleanSession = false;
    if (client.connect("lolinClient")) {
      Serial.println("connected");
      client.subscribe("room/r1");
      client.subscribe("room/r2");
      client.subscribe("room/r3");
      client.subscribe("room/r4");
      client.subscribe("sys/heartbeat");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);

    }
  }
}
void loop(void) {
  server.handleClient();
  delay(1);
  if (now2 = millis() - lastHeartbeat < 3000) {
    heartbeatServerOn = false;
    Serial.println("Connection lost, system restarting in 2s");
    client.publish("sys/cardiacArrest", "1");

    while (now2 + 5000 > millis()) {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(250);
      digitalWrite(LED_BUILTIN, LOW);
    }
    reconnect();
  }


  if (!client.connected()) {
    reconnect();

  }
  if (!client.loop())
    client.connect("lolinClient");

  now = millis();
  // Publishes new temperature and humidity every 2 seconds
  if (now - lastMeasure > 2000) {
    lastMeasure = now;
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    // Check if any reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
      Serial.println("DHT Sensor disconnected");
      digitalWrite(LED_BUILTIN, HIGH);
    } else {
      digitalWrite(LED_BUILTIN, LOW);

      // Computes temperature values in Celsius
      float hic = dht.computeHeatIndex(t, h, false);

      static char heatIndex[7];
      dtostrf(hic, 6, 2, heatIndex);

      static char temperatureTemp[7];
      dtostrf(t, 6, 2, temperatureTemp);

      static char humidityTemp[7];
      dtostrf(h, 6, 2, humidityTemp);

      client.publish("room/temperature", temperatureTemp);
      client.publish("room/heatIndex", heatIndex);
      client.publish("room/humidity", humidityTemp);

      Serial.print("<3Beat | ");
      Serial.print("Humidity: ");
      Serial.print(h);
      Serial.print(" %  Temperature: ");
      Serial.print(t);
      Serial.print(" *C  Heat Index: ");
      Serial.print(hic);
      Serial.print(" *C  ");
    }

    int light = analogRead(ldr);
    float lightVolt = light * (5.0 / 1023.0);
    static char lightV[5];
    dtostrf(lightVolt, 4, 2, lightV);

    client.publish("room/light", lightV);

    Serial.print("Light: ");
    Serial.print(lightV);
    if (digitalRead(pir) == LOW) {
      client.publish("room/motion", "1");
      Serial.println("  Motion");
    } else {
      client.publish("room/motion", "0");
      Serial.println("  No Motion");
    }
    client.publish("sys/cardiacArrest", "0");
  }
}
