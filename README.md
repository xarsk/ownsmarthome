# OwnSmartHome

Starting from a relay board, with ambitions to create an open-source home assistant.

Currently, Wemos Lolin 32 board is used. This setup uses a 4-channel relay board,
a PIR sensor, an LDR, and a digital environment sensor (DHT22). 
Server uses Node-RED and Mosquitto broker, load that a RPi can easily handle.
